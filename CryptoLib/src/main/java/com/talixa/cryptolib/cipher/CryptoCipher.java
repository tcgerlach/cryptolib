package com.talixa.cryptolib.cipher;

import java.security.Provider;
import java.security.Security;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.talixa.cryptolib.cipher.parameters.CryptoParameters;

/**
 * Base class for all cryptography ciphers.
 * 
 * @author tcgerlach
 */
public abstract class CryptoCipher {
	
	protected String cipherName;	
	protected CryptoParameters params;
	protected final static String BOUNCY_CASTLE_PROVIDER = "org.bouncycastle.jce.provider.BouncyCastleProvider";
			
	/**
	 * Construct a cipher using the specified algorithm
	 * 
	 * @param algorithm
	 */
	public CryptoCipher(String algorithm, CryptoParameters param) {
		this.cipherName = algorithm;
		this.params = param;
	}
	
	/**
	 * Returns the name of the cipher algorithm
	 * 
	 * @return cipher name
	 */
	public String getCipherName() {
		return cipherName;
	}
	
	/**
	 * Returns the crypto parameters
	 * 
	 * @return the parameters of the crypto system
	 */
	
	public CryptoParameters getCryptoParameters() {
		return params;
	}
	
	/**
	 * Encrypts the plaintext
	 * 
	 * @param plaintext data to encrypt 
	 * @return ciphertext
	 * @throws CryptoException
	 */
	public abstract byte[] encrypt(byte[] plaintext); 
	
	/**
	 * Decrypts the ciphertext 
	 * 
	 * @param ciphertext data to decrypt
	 * @return plaintext
	 * @throws CryptoException
	 */
	public abstract byte[] decrypt(byte[] ciphertext);
	
	/**
	 * Returns a list of all ciphers available on the system
	 * 
	 * @return list of ciphers
	 */
	@SuppressWarnings("rawtypes")
	public static String[] getAvailableCiphers() {
		String service = "Cipher";    	    	
		Set<String> result = new HashSet<String>();
		
		// All all providers
		Provider[] providers = Security.getProviders();
		for (int i=0; i<providers.length; i++) {
			// Get services provided by each provider
			Set keys = providers[i].keySet();
			for (Iterator it=keys.iterator(); it.hasNext(); ) {
				String key = (String)it.next();
				key = key.split(" ")[0];
				
				if (key.startsWith(service + ".")) {
					result.add(key.substring(service.length()+1));
				} else if (key.startsWith("Alg.Alias."+service+".")) {
					// This is an alias
					result.add(key.substring(service.length()+11));
				}
			}
		}
		return (String[])result.toArray(new String[result.size()]);
	}
}