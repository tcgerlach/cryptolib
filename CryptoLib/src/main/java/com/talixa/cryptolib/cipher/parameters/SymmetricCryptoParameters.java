package com.talixa.cryptolib.cipher.parameters;

/**
 * Parameters for symmetric encryption/decryption
 * 
 * @author tcgerlach
 */
public class SymmetricCryptoParameters extends CryptoParameters {

	private String password;

	/**
	 * Creates a new set of crypto parameters
	 * 
	 * @param password the secret key for symmetric encryption/decryption
	 */
	public SymmetricCryptoParameters(String password) {
		super();
		this.password = password;
	}
	
	/**
	 * Creates a blank set of crypto parameters
	 */
	public SymmetricCryptoParameters() {
		super();
	}
	
	/**
	 * Returns the secret password
	 * 
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	
	/**
	 * Sets the symmetric password
	 * 
	 * @param password new secret key
	 */
	public void setPassword(String password) {
		this.password = password;
	}
}
