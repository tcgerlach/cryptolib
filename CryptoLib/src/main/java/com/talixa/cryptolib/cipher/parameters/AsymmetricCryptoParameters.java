package com.talixa.cryptolib.cipher.parameters;

import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * Contains the parameters necessary for asymmetric encryption
 * 
 * @author tcgerlach
 */
public class AsymmetricCryptoParameters extends CryptoParameters{

	public enum EncryptionMode {PUBLIC_KEY, PRIVATE_KEY};
	
	private PrivateKey privkey;
	private PublicKey  pubkey;
	private EncryptionMode encMode;	
	
	/**
	 * Creates a blank set of parameters	
	 */
	public AsymmetricCryptoParameters() {
		super();
	}
	
	/**
	 * Creates a set of crypto parameters
	 * 
	 * @param privkey private key for encryption/decryption
	 * @param pubkey public key for encryption/decryption
	 * @param encMode determines which key is used for encryption
	 */
	public AsymmetricCryptoParameters(PrivateKey privkey, PublicKey pubkey,	EncryptionMode encMode) {
		super();
		this.privkey = privkey;
		this.pubkey = pubkey;
		this.encMode = encMode;
	}
	
	/**
	 * Returns the encryption mode
	 * 
	 * @return the encryption mode
	 */
	public EncryptionMode getEncryptionMode() {
		return encMode;
	}
	
	/**
	 * Sets the encryption mode
	 * 
	 * @param encMode mode for encryption
	 */
	public void setEncryptionMode(EncryptionMode encMode) {
		this.encMode = encMode;
	}
	
	/**
	 * Returns the private key
	 * 
	 * @return the private key
	 */
	public PrivateKey getPrivateKey() {
		return privkey;
	}
	
	/**
	 * Sets the private key
	 * 
	 * @param privkey new private key
	 */
	public void setPrivateKey(PrivateKey privkey) {
		this.privkey = privkey;
	}
	
	/**
	 * Returns the public key'
	 * 
	 * @return the public key
	 */
	public PublicKey getPublicKey() {
		return pubkey;
	}
	
	/**
	 * Sets the public key 
	 * 
	 * @param pubkey the new public key
	 */
	public void setPublicKey(PublicKey pubkey) {
		this.pubkey = pubkey;
	}
}
