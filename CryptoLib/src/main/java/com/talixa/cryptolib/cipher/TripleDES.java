package com.talixa.cryptolib.cipher;

import com.talixa.cryptolib.cipher.parameters.SymmetricCryptoParameters;

/**
 * Cipher implementation of Triple DES
 * 
 * @author tcgerlach
 */
public class TripleDES extends DES {		
	public TripleDES(SymmetricCryptoParameters params) {
		super("PBEWithMD5AndTripleDES", params);
	}		
}
