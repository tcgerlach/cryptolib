package com.talixa.cryptolib.cipher;

import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import com.talixa.cryptolib.cipher.parameters.SymmetricCryptoParameters;

/**
 * Cipher implementation of AES
 * 
 * @author tcgerlach
 */
public class AES extends CryptoCipher {		
	
	// Password salt
	private static byte[] SALT = {0x15, 0x0B, 0x74, 0x17, 0x4C, 0x2C, 0x73, 0x1F};
	private static final int KEY_GENERATOR_ITERATION_COUNT = 256;
	
	// cipher instances for enciphering and deciphering
	private Cipher ecipher;
	private Cipher dcipher;
	
	public AES(SymmetricCryptoParameters params) {				
		super("AES/CBC/PKCS5Padding", params);
		String password = params.getPassword();
		
        try { 
        	SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
    		KeySpec spec = new PBEKeySpec(password.toCharArray(), SALT, KEY_GENERATOR_ITERATION_COUNT, 256);
    		SecretKey tmp = factory.generateSecret(spec);
    		SecretKey secret = new SecretKeySpec(tmp.getEncoded(), "AES");
        	
        	ecipher = Cipher.getInstance(secret.getAlgorithm());
        	dcipher = Cipher.getInstance(secret.getAlgorithm());

        	// Init the ciphers
        	ecipher.init(Cipher.ENCRYPT_MODE, secret);
        	dcipher.init(Cipher.DECRYPT_MODE, secret);
        } catch (Exception e) {
        	throw new RuntimeException(e);
        }
	}
	
	public byte[] encrypt(byte[] data) {	
		try {
			return ecipher.doFinal(data);		
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

    public byte[] decrypt(byte[] data) {    
		try {
			return dcipher.doFinal(data);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
    }	
}
