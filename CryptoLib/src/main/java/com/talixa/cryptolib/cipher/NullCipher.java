package com.talixa.cryptolib.cipher;

import com.talixa.cryptolib.cipher.parameters.CryptoParameters;

/**
 * Null Cipher implementation.
 * Encrypting and decrypting return the data unmodified. 
 * 
 * @author tcgerlach
 */
public class NullCipher extends CryptoCipher {

	public NullCipher(CryptoParameters params) {
		super("NullCipher", params);		
	}

	public byte[] encrypt(byte[] plaintext) {
		return plaintext;
	}
	
	public byte[] decrypt(byte[] ciphertext) {
		return ciphertext;
	}
}
