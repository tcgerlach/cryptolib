package com.talixa.cryptolib.cipher;

import com.talixa.cryptolib.cipher.parameters.SymmetricCryptoParameters;

/**
 * Cipher implementation of Standard DES
 * 
 * @author tcgerlach
 */
public class StandardDES extends DES {	
	public StandardDES(SymmetricCryptoParameters params) {
		super("PBEWithMD5AndDES", params);
	}		
}
