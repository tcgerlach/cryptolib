package com.talixa.cryptolib.cipher;

import java.security.Provider;
import java.security.Security;

import javax.crypto.Cipher;

import com.talixa.cryptolib.cipher.parameters.AsymmetricCryptoParameters;
import com.talixa.cryptolib.cipher.parameters.AsymmetricCryptoParameters.EncryptionMode;

/**
 * RSA cipher using the BouncyCastleProvider
 * 
 * @author tcgerlach
 */
public class RSA extends CryptoCipher {
	
	private Cipher ecipher;
	private Cipher dcipher;
	private AsymmetricCryptoParameters params;	
	
	public RSA(AsymmetricCryptoParameters cryptoParams) {		
		super("RSA", cryptoParams);		
		this.params = cryptoParams;
		try {
			Provider rsaProvider = (Provider)Class.forName(CryptoCipher.BOUNCY_CASTLE_PROVIDER).newInstance();			
			Security.addProvider(rsaProvider);
			ecipher = Cipher.getInstance("RSA/None/OAEPPadding");
			dcipher = Cipher.getInstance("RSA/None/OAEPPadding");	
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}
	
	public byte[] encrypt(byte[] plaintext) {	
		try {		
			if (params.getEncryptionMode() == EncryptionMode.PRIVATE_KEY) {
				ecipher.init(Cipher.ENCRYPT_MODE, params.getPrivateKey());				
			} else {
				ecipher.init(Cipher.ENCRYPT_MODE, params.getPublicKey());
			}
			
			byte[] returndata = ecipher.doFinal(plaintext);		
			return returndata; 
		} catch (Exception e) {			
			throw new RuntimeException (e);
		} 
	}

	public byte[] decrypt(byte[] ciphertext) {		
		try {
			if (params.getEncryptionMode() == EncryptionMode.PRIVATE_KEY) {
				dcipher.init(Cipher.DECRYPT_MODE, params.getPublicKey());				
			} else {
				dcipher.init(Cipher.DECRYPT_MODE, params.getPrivateKey());
			}
			byte[] returndata = dcipher.doFinal(ciphertext);		
			return returndata;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
