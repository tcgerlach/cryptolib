package com.talixa.cryptolib.cipher;

import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

import com.talixa.cryptolib.cipher.parameters.SymmetricCryptoParameters;

/**
 * Base DES implementation. 
 * 
 * @author tcgerlach
 */
public abstract class DES extends CryptoCipher {
	
	// Password salt
	private static byte[] SALT = {0x49, 0x5B, 0x28, 0x32, 0x56, 0x35, 0x43, 0x03};
	private static final int KEY_GENERATOR_ITERATION_COUNT = 15;
	
	// cipher instances for enciphering and deciphering
	private Cipher ecipher;
	private Cipher dcipher;
	
	public DES(String algorithm, SymmetricCryptoParameters params) {				
		super(algorithm, params);
		String password = params.getPassword();
        KeySpec keySpec = new PBEKeySpec(password.toCharArray(), SALT, KEY_GENERATOR_ITERATION_COUNT);
        try { 
        	SecretKey key = SecretKeyFactory.getInstance(algorithm).generateSecret(keySpec);
        	
        	ecipher = Cipher.getInstance(key.getAlgorithm());
        	dcipher = Cipher.getInstance(key.getAlgorithm());

        	// Prepare the parameter to the ciphers
        	AlgorithmParameterSpec paramSpec = new PBEParameterSpec(SALT, KEY_GENERATOR_ITERATION_COUNT);

        	// Init the ciphers
        	ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
        	dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
        } catch (Exception e) {
        	throw new RuntimeException(e);
        }
	}
	
	public byte[] encrypt(byte[] data) {	
		try {
			return ecipher.doFinal(data);		
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

    public byte[] decrypt(byte[] data) {    
		try {
			return dcipher.doFinal(data);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
    }	
}
