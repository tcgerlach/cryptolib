package com.talixa.cryptolib;

import com.talixa.cryptolib.CryptoToolkit.Stream;
import com.talixa.cryptolib.stream.CryptoStream;

/**
 * Provides some basic tools for steganography use
 * 
 * @author tcgerlach
 */
public class StegoToolkit {
	
	private static final Stream STREAM_GEN = Stream.FIBONACCI;

	/**
	 * Sets the last bit in each byte of data to 0.
	 * 
	 * @param data data to remove last bit from
	 * @return data with last bit set to 0
	 */
	public static byte[] removeLastBit(byte[] data) {
		for(int i = 0; i < data.length; ++i) {
			data[i] = (byte)(((int)data[i]) & 0xFE);
		}
		return data;
	}
	
	/**
	 * Retrieves a short hidden in the data
	 * 
	 * @param data data to retrieve short from
	 * @param startPosition first byte containing short data
	 * @return short found in data
	 */
	public static short retrieveShort(byte[] data, int startPosition) {		
		StringBuilder shortString = new StringBuilder();
		for (int i = startPosition; i < (startPosition+16); ++i) {
			if ((data[i] & 0x01) == 0x01) {
				shortString.append("1");
			} else {
				shortString.append("0");
			}
		}
		
		short returnvalue = Short.parseShort(shortString.toString(),2);
		if ((data[0] & 0x01) == 0x01) {
			returnvalue |= 0x8000;
		}
		return returnvalue;
	}	
	
	/**
	 * Retrieves a long hidden in the data
	 * 
	 * @param data data to retrieve long from
	 * @param startPosition first byte containing long data
	 * @return long found in data
	 */
	public static long retrieveLong(byte[] data, int startPosition) {		
		StringBuilder longString = new StringBuilder();
		for (int i = startPosition; i < (startPosition+64); ++i) {
			if ((data[i] & 0x01) == 0x01) {
				longString.append("1");
			} else {
				longString.append("0");
			}
		}
		
		long returnvalue = Long.parseLong(longString.toString(),2);
		if ((data[0] & 0x01) == 0x01) {
			returnvalue |= 0x8000000000000000L;
		}
		return returnvalue;
	}	
	
	/**
	 * Retrieves an int hidden in the data
	 * 
	 * @param data data to retrieve int from
	 * @param startPosition first byte containing int data
	 * @return int found in data
	 */
	public static int retrieveInt(byte[] data, int startPosition) {			
		StringBuilder intString = new StringBuilder();
		for (int i = startPosition; i < (32+startPosition); ++i) {
			if ((data[i] & 0x01) == 0x01) {
				intString.append("1");
			} else {
				intString.append("0");
			}
		}
		
		int returnvalue = Integer.parseInt(intString.toString(),2);
		if ((data[0] & 0x01) == 0x01) {
			returnvalue |= 0x80000000;
		}
		return returnvalue;
	}	
		
	/**
	 * Changes a short to a sequence of 16 bytes for adding to a data stream
	 * 
	 * @param shortValue short to turn to bytes
	 * @return a byte[16] with each byte containing one bit of the short
	 */
	public static byte[] createStegoBytesFromShort(int shortValue) {
		return createStegoBytesFromIntegralType(shortValue, 16);
	}
	
	/**
	 * Changes an int to a sequence of 32 bytes for adding to a data stream
	 * 
	 * @param intValue int to turn to bytes
	 * @return a byte[32] with each byte containing one bit of the int
	 */
	public static byte[] createStegoBytesFromInt(int intValue) {
		return createStegoBytesFromIntegralType(intValue, 32);
	}
	
	/**
	 * Changes a long to a sequence of 64 bytes for adding to a data stream
	 * 
	 * @param longValue long to turn to bytes
	 * @return a byte[64] with each byte containing one bit of the long
	 */
	public static byte[] createStegoBytesFromLong(long longValue) {
		return createStegoBytesFromIntegralType(longValue, 64);
	}
	
	private static byte[] createStegoBytesFromIntegralType(long longValue, int size) {
		byte[] checkSumBytes = new byte[size];
		for(int i = 0; i < size; ++i) {
			checkSumBytes[i] = (byte)((longValue >> (size-1)-i) & 0x01);
		}
		return checkSumBytes;
	}
	
	/**
	 * Adds a watermark to the data.  This watermark consists of a checksum and 
	 * a the output from a stream generator.
	 * 
	 * @param data data to watermark
	 * @return watermarked data
	 */
	public static byte[] addSecurityWatermark(byte[] data) {
		StegoToolkit.removeLastBit(data);		
		byte[] checkSumBytes = StegoToolkit.createStegoBytesFromLong(calcCheckSum(data));		
		CryptoStream lfsr = CryptoToolkit.getStream(STREAM_GEN, (int)System.currentTimeMillis());
		for(int i = 0; i < data.length; ++i) {
			if (i < checkSumBytes.length) {
				data[i] = (byte)(data[i] | (int)checkSumBytes[i]);
			} else {
				int nextBit = lfsr.getNextBit();				
				data[i] = (byte)(data[i] | nextBit);
			}			
		}	
		return data;	
	}			

	/**
	 * Checks data to see if it was modified.
	 * 
	 * @param data data to check
	 * @return true if the data is unmodified
	 */
	public static boolean checkSecurityWatermark(byte[] data) {
		byte[] dataCopy = new byte[data.length];
		System.arraycopy(data, 0, dataCopy, 0, data.length);		
		long signatureChecksum = StegoToolkit.retrieveLong(dataCopy, 0);
		StegoToolkit.removeLastBit(dataCopy);
		long calculatedChecksum = calcCheckSum(dataCopy);
		boolean lfsrCheckGood = checkWatermarkLFSR(data, STREAM_GEN, 64, 1);
		return signatureChecksum == calculatedChecksum && lfsrCheckGood;
	}
	
	/**
	 * Checks an imbedded LFSR watermark
	 * 
	 * @param data data to check
	 * @param streamGenerator type of stream generator to be used
	 * @param startByte byte to begin check on
	 * @param seed initial seed for LFSR
	 * @return true if watermark is good
	 */
	public static boolean checkWatermarkLFSR(byte[] data, Stream streamGenerator, int startByte, int seed) {			
		CryptoStream lfsr = CryptoToolkit.getStream(streamGenerator, seed);
		for(int i = startByte; i < data.length; ++i) {
			int nextBit = lfsr.getNextBit();
			if ((data[i] & 0x01) != nextBit) {
				return false;
			}
		}
		return true;		
	}
	
	private static long calcCheckSum(byte[] data) {				
		long checkSum = 0;
		for(int i = 0; i < data.length; ++i) {
			checkSum = (data[i] << (i % 56)) ^ checkSum;					
		}
		checkSum = Math.abs(checkSum);	// I don't like negative checksums...
		return checkSum;
	}	
}