package com.talixa.cryptolib;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.security.KeyFactory;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;

/**
 * @author tcgerlach
 */
public class PKIToolkit {
	
	/**
	 * Loads an X.509 Certificate from file
	 * 
	 * @param certFile name of X.509 certificate file
	 * @return the X.509 certificate loaded from the file
	 * @throws CertificateException certificate was invalid
	 * @throws IOException 
	 */
	public static Certificate loadX509Certificate(String certFile) throws CertificateException, IOException {	
		FileInputStream is = null;
		try {
			is = new FileInputStream(certFile);
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			Certificate cert = cf.generateCertificate(is);	
			return cert;
		} finally {
			if (is != null) {
				is.close();
			}
		}
	}
	
	/**
	 * Loads an unencrypted PKCS#8 private key from a file
	 * 
	 * @param pkcs8keyfile key file name
	 * @return an RSAPrivateKey object
	 * @throws IOException 
	 * @throws CryptoException
	 */
	public static RSAPrivateKey loadRSAPrivateKey(String pkcs8keyfile) throws IOException {
		RSAPrivateKey rsaPrivKey = null;
		try {
			// read key file
			File file = new File(pkcs8keyfile);	
			InputStream is = new FileInputStream(file);		
			byte[] data = new byte[(int)file.length()];		
			is.read(data);
			
			// remove all the text from the key file
			String dataString = new String(data, Charset.forName("US-ASCII"));
			dataString = dataString.replaceAll("-----BEGIN PRIVATE KEY-----", "");
			dataString = dataString.replaceAll("-----END PRIVATE KEY-----", "");
			dataString = dataString.replaceAll("\n", "").replaceAll("\r", "").replaceAll(" ", "");	
			
			// decode base 64
			byte[] keyBytes = Base64Encoder.decodeBase64(dataString);
					
			// get key factory and create java key
			KeyFactory rsakf = KeyFactory.getInstance("RSA");
			PKCS8EncodedKeySpec privKeySpec = new PKCS8EncodedKeySpec(keyBytes);
			rsaPrivKey= (RSAPrivateKey) rsakf.generatePrivate(privKeySpec);		
			
			// close file
			is.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		return rsaPrivKey;
	}
}
