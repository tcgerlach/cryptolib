package com.talixa.cryptolib.digest;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.Security;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Base class for all crypto digests
 * 
 * @author tcgerlach
 */
public class CryptoDigest {
	protected String algorithm;
	
	/**
	 * Create a digest using the specified algorithm
	 * 
	 * @param algorithm digest name
	 */
	public CryptoDigest(String algorithm) {
		this.algorithm = algorithm;
	}
	
	/**
	 * Returns the digest of the given data
	 * 
	 * @param msg data to generate digest of
	 * @return message digest
	 * @throws NoSuchAlgorithmException 
	 */
	public byte[] digest(byte[] msg) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance(algorithm);
		md.update(msg);
		return md.digest();
	}
	
	/**
	 * Returns a list of all digests available on the system
	 * 
	 * @return list of digests
	 */
	@SuppressWarnings("rawtypes")
	public static String[] getAvailableDigests() {
		String service = "MessageDigest";    	    	
		Set<String> result = new HashSet<String>();
		
		// All all providers
		Provider[] providers = Security.getProviders();
		for (int i=0; i<providers.length; i++) {
			// Get services provided by each provider
			Set keys = providers[i].keySet();
			for (Iterator it=keys.iterator(); it.hasNext(); ) {
				String key = (String)it.next();
				key = key.split(" ")[0];
				
				if (key.startsWith(service + ".")) {
					result.add(key.substring(service.length()+1));
				} else if (key.startsWith("Alg.Alias."+service+".")) {
					// This is an alias
					result.add(key.substring(service.length()+11));
				}
			}
		}
		return (String[])result.toArray(new String[result.size()]);
	}
}
