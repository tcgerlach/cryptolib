package com.talixa.cryptolib.digest;

/**
 * @author tcgerlach
 */
public class SHA256Digest extends CryptoDigest{
	public SHA256Digest() {
		super("SHA-256");
	}	
}
