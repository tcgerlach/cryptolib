package com.talixa.cryptolib.digest;

/**
 * Digest implementation of MD5 
 * 
 * @author tcgerlach
 */
public class MD5Digest extends CryptoDigest{
	public MD5Digest() {
		super("MD5");
	}	
}
