package com.talixa.cryptolib.digest;

/**
 * @author tcgerlach
 */
public class SHA384Digest extends CryptoDigest{
	public SHA384Digest() {
		super("SHA-384");
	}	
}
