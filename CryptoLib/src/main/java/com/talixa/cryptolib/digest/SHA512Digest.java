package com.talixa.cryptolib.digest;

/**
 * @author tcgerlach
 */
public class SHA512Digest extends CryptoDigest{
	public SHA512Digest() {
		super("SHA-512");
	}
}
