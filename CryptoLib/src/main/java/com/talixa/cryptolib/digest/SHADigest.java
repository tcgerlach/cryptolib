package com.talixa.cryptolib.digest;

/**
 * Digest implementation of SHA1 
 * 
 * @author tcgerlach
 */
public class SHADigest extends CryptoDigest {
	public SHADigest() {
		super("SHA");
	}	
}
