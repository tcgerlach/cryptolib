package com.talixa.cryptolib.stream;

/**
 * Base class of a Linear Feedback Shift Register Generator
 * 
 * @author tcgerlach
 */
public abstract class CryptoStream {

	protected long register;
	
	public int getRegister() {		
		return (int)register;
	}
			
	public abstract int getNextBit();
	
	public int getNextInt() {
		for(int i = 0; i < 32; ++i) {
			getNextBit();
		}
		return getRegister();
	}
	
	public double detectBias(int iterations) {
		int onesCount = 0;
		int zeroesCount = 0;
		
		for(int i = 0; i < iterations; ++i) {
			if (this.getNextBit() == 1) {
				++onesCount;
			} else {
				++zeroesCount;			
			}
		}
		return (double)onesCount / ((double)onesCount + (double)zeroesCount);
	} 
	
	public void setRegister(int register) {			
		if (register == 0) {
			// Stream seed cannot be 0 - change to 1
			register = 1;
		}
		
		/*
		 * since Java doesn't do unsigned numbers, the only way to get
		 * an identical binary representation between an int and a long
		 * is to treat everything as a binary string 
		 */
		
		this.register = Long.parseLong(Integer.toBinaryString(register),2);													
	}
}