package com.talixa.cryptolib.stream;

/**
 * Implements a Galois Linear Feedback Shift Register 
 * with poly (32,7,5,3,2,1,0).
 *   
 * This is faster than the FibonacciGenerator, but less commonly used
 * 
 * @author tcgerlach
 */
public class GaloisGenerator extends CryptoStream {

	private int mask = 0x80000057;
	
	public GaloisGenerator() {	
		this(1);		
	}
	
	public GaloisGenerator(int seed) {			
		setRegister(seed);
	}
	
	public int getNextBit() {						
		int nextBit;
		if ((register & 0x00000001) == 1) {
			register = (((register ^ mask) >>> 1) | 0x8000000);
			nextBit = 1;
		} else {
			register >>>=1;
			nextBit = 0;
		}
		return nextBit;
	}

}
