package com.talixa.cryptolib.stream;

/**
 * This is a null stream; every bit is a 0
 * 
 * @author tcgerlach
 */
public class NullGenerator extends CryptoStream {

	public NullGenerator() {	
		this(1);		
	}
	
	public NullGenerator(long seed) {	
		// do nothing
	}
	
	public int getNextBit() {
		return 0;
	}
}
