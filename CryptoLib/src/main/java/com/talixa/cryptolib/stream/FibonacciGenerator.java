package com.talixa.cryptolib.stream;

/**
 * Implements a Linear Feedback Shift Register 
 * with poly (32,7,5,3,2,1,0).  This is a max length
 * poly generating 2^32 -1 bits.
 * 
 * This is slow, but is the standard LFSR
 * 
 * @author tcgerlach
 */
public class FibonacciGenerator extends CryptoStream {
	
	public FibonacciGenerator() {		
		this(1);
	}
	
	public FibonacciGenerator(int seed) {			
		setRegister(seed);
	}
	
	int bitCount = 0;
	
	public int getNextBit() {		
		int retVal;
		
		/*
		 * NOTE:
		 * I can't figure out how to get the register from the LFSR output.
		 * This register value will be required to sync a second LFSR off the first.
		 * So, to make things easy, the LFSR will start by outputing it's seed
		 * then, anything that has to sync off of the LFSR can easily sync up by
		 * setting it's own seed to the first 32 bits of the other LFSR.		 
		 */
		
		if (bitCount < 32) {
			retVal = (int)(register >>> (31-bitCount)) & 0x01;
			++bitCount;
		} else {
			register = (((((register >>> 31) 
				^ (register >>> 6)
				^ (register >>> 4) 
				^ (register >>> 2)
				^ (register >>> 1)
				^ register))
				& 0x01)
				<< 31)
				| (register >>> 1);
			retVal = (int)(register & 0x01);
		}
		return retVal;
	}
}
