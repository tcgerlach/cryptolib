package com.talixa.cryptolib;

public class Base64Encoder {
	/*
	 * To convert data to base64, the first byte is placed in the most significant eight bits of a 24-bit buffer, 
	 * the next in the middle eight, and the third in the least significant eight bits. If there are fewer than 
	 * three bytes left to encode (or in total), the remaining buffer bits will be zero. The buffer is then used, 
	 * six bits at a time, most significant first, as indices into the string: 
	 * "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/" and the indicated character is output.
	 * 
	 * The process then repeats on the remaining input data.
	 * 
	 * If there are two input bytes remaining (the remainder of the total input bytes divided by three is two), pad 
	 * with one "=". If there is one input byte remaining (remainder was one), pad with two "=", otherwise, don't pad. 
	 * This prevents extra bits being added to the reconstructed data.
	 */
	private final static String lookup = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	
	/**
	 * Encodes data to base64 format
	 * 
	 * @param rawdata data to be encoded 
	 * @return encoded data
	 */
	public static String encodeBase64(byte[] rawdata) {
		StringBuffer base64 = new StringBuffer();
	
		for(int dataIndex = 0; dataIndex < rawdata.length; dataIndex+=3) {			
			// base64[0] will be the first 6 bits of data[0] 
			base64.append(lookup.charAt(rawdata[dataIndex] >> 2 & 0x3F));			
			
			// base64[1] will be the last 2 bits of data[0] and the first 4 bits if data[1]
			if (dataIndex+1 >= rawdata.length) {
				base64.append(lookup.charAt(((rawdata[dataIndex] & 0x03) << 4) | 0x00));
			} else {
				base64.append(lookup.charAt(((rawdata[dataIndex] & 0x03) << 4) | ((rawdata[dataIndex+1] >> 4) & 0x0F)));
			}			
			
			// base64[2] will be the last 4 bits of data[1] + the first 2 bits of data[3]
			if (dataIndex+1 >= rawdata.length) {
				base64.append('=');
			} else if (dataIndex+2 >= rawdata.length){
				base64.append(lookup.charAt(((rawdata[dataIndex+1] & 0x0F) << 2) | 0x00));
			} else {
				base64.append(lookup.charAt(((rawdata[dataIndex+1] & 0x0F) << 2) | ((rawdata[dataIndex+2] >> 6) & 0x03)));
			}			
			
			// base64[3] will be the first 6 bytes of data[0]
			if (dataIndex+2 >= rawdata.length) {
				base64.append('=');
			} else {
				base64.append(lookup.charAt(rawdata[dataIndex+2] & 0x3f));
			}
			
		}
		
		return base64.toString();
	}
	
	/**
	 * Decodes base64 data
	 * 
	 * @param base64 encoded data
	 * @return decoded data
	 */
	public static byte[] decodeBase64(String base64) {
		int dataSize = base64.length() * 3/4;
		if (base64.charAt(base64.length()-2) == '=') {
			dataSize -= 2;
		} else if (base64.charAt(base64.length()-1) == '=') {
			dataSize -= 1;
		}	
		byte[] data = new byte[dataSize];
		
		int dataIndex = 0;
		
		for(int base64index = 0; base64index < base64.length(); base64index+=4) {
			// data[0] will be the all 6 bits of base64[0] + the first 2 bits of base64[1]
			data[dataIndex++] = (byte)((lookup.indexOf(base64.charAt(base64index)) << 2 & 0xFC)| (lookup.indexOf(base64.charAt(base64index+1)) >> 4 & 0x03));			
			
			// data[1] will be the last 4 bits of base64[1] + the first 4 bits of base64[2]
			if (base64.charAt(base64index+2) == '=') {				
				break;
			}
			data[dataIndex++] = (byte)((lookup.indexOf(base64.charAt(base64index+1)) << 4 & 0xF0) | (lookup.indexOf(base64.charAt(base64index+2)) >> 2 & 0x0F));			
			
			// data[2] will be the last 2 bits of base64[2] + the first 6 bits of base64[3]
			if (base64.charAt(base64index+3) == '=') {
				break;
			}
			data[dataIndex++] = (byte)((lookup.indexOf(base64.charAt(base64index+2)) << 6 & 0xC0) | (lookup.indexOf(base64.charAt(base64index+3)) & 0x3F));		
		}						
		return data;
	}
}
