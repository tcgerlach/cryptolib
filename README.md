Various cryptography functions and tools.  
This package requires BouncyCastle at runtime.  
1. Code to simplify usage of DES, 3DES, RSA  
2. Code to simplify usage of MD5 and SHA  
3. Code for cipher streams including Galois and Fibonacci  
4. Toolkit for basic steganography  
5. Toolkit for dealing with PKI data  
